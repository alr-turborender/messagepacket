//**************************************************************************/
// DESCRIPTION: MessagePacket. Class to work with messages format of key-value
// AUTHOR: Falkovski Alexander
// (C)2014
//***************************************************************************/

#include <MessagePacket.h>

MessagePacket::MessagePacket(void)
{

}
MessagePacket::MessagePacket(std::string const &data)
{
    FillPacket(data);
}
MessagePacket::MessagePacket(std::string const &handle, PacketHeader::PacketType t)
{
    if(handle.length() > HandleSize)
        throw new std::exception("MessagePacket() : Handle to large");

    SetHandle(handle);
    SetType(t);
}
MessagePacket::~MessagePacket(void)
{
    Clear();
}
void MessagePacket::ParseHeader(std::string const &data)
{
    const char * ExceptionMessage = "MessagePacket() : Bad header";
    if(data.length() < HeaderSize)
        throw std::exception(ExceptionMessage);

    void * header_ptr = const_cast<char *>(data.data());
    void * type_ptr = static_cast<char *>(header_ptr) + sizeof(Handle);
    char end_header = static_cast<char>(*(static_cast<char *>(type_ptr) + sizeof(Type)));
    if(end_header != EOH)
        throw std::exception(ExceptionMessage);

    memcpy_s(Handle, sizeof(Handle), header_ptr, sizeof(Handle));
    memcpy_s(&Type, sizeof(Type), type_ptr, sizeof(Type));
}
void MessagePacket::ParseParams(std::string const &data)
{
    Params.clear();
    if(data.size() <= HeaderSize)
        return;

    std::string Msg(data.data() + HeaderSize, data.data() + data.size());
    std::string param, value;
    int endval;
    int endblock;
    int offset = 0;
    while((endblock = Msg.find_first_of(EndOfValue)) != std::string::npos)
    {
        endval = Msg.find(EndOfParam);
        if(endblock <= endval)
        {
            Msg = Msg.substr(endblock + 1);
            offset += endblock + 1;
            continue;
        }
        param = Msg.substr(0, endval);

        Msg = Msg.substr(endval + 1);
        offset += endval + 1;

        endval = Msg.find(EndOfValue);
        value = Msg.substr(0, endval);

        Params.insert(ParamList::value_type(param, value));

        Msg = Msg.substr(endval + 1);
        offset += endval + 1;
    }
}
void MessagePacket::FillPacket(std::string const &data)
{
    try
    {
        ParseHeader(data);
        ParseParams(data);
    }catch(std::exception &)
    {
        Clear();
    }
    
}
bool MessagePacket::AddParam(std::string const &param, std::string const &value)
{
    if(value.length() == 0)
        return false;

    int oldSize = Params.size();
    Params.insert(ParamList::value_type(param, value));
    if(oldSize == Params.size())
        return false;

    return true;
}
bool MessagePacket::AddParam(std::string const &value)
{
    return AddParam("", value);
}
void MessagePacket::ClearParams()
{
    Params.clear();
}
void MessagePacket::Clear()
{
    Params.clear();
    IpAddress.clear();
}

std::string MessagePacket::operator[](std::string const &param) const
{
    ParamList::const_iterator it = Params.find(param);
    if(it == Params.end())
        return std::string();
    else
        return it->second;
}
std::string MessagePacket::operator[](size_t nparam) const
{
    if(Params.size() > nparam)
        for(ParamList::const_iterator it = Params.begin(); it != Params.end(); it++)
            if(nparam-- == 0)
                return it->second;

        return std::string();
}
MessagePacket& MessagePacket::operator=(std::string const &data)
{
    this->FillPacket(data);
    return *this;
}
MessagePacket& MessagePacket::operator=(MessagePacket const &packet)
{
    this->FillPacket(packet.GetPrintData());
    return *this;
}
std::string MessagePacket::GetPrintData() const
{
    std::string ret(Handle, &Handle[HandleSize]);
    char type_ch[sizeof(PacketType)] = {0};
    memcpy_s(type_ch, sizeof(type_ch), &Type, sizeof(Type));
    ret.append(type_ch, &type_ch[sizeof(PacketType)]);
    ret.push_back(EndOfHeader);
    for(ParamList::const_iterator it = Params.begin(); it != Params.end(); it++)
    {
        ret.append(it->first);
        ret.push_back(EndOfParam);
        ret.append(it->second);
        ret.push_back(EndOfValue);
    }
    return ret;
}
